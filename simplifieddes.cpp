#include "simplifieddes.h"
#include <iostream>
using namespace std;

SimplifiedDES::SimplifiedDES()
{
    key="0000000000";
}
SimplifiedDES::SimplifiedDES(string key):
    key(key)
{
}

void SimplifiedDES::test()
{
    cout<<"from lib S-DES"<<endl<<"\n";
    bitset<8> plaintext(string("11001100"));
    bitset<8> ciphertext;
    bitset<8> K1, K2;
    cout<<"Plaintext: "<<plaintext<<endl;
    cout<<"key:       "<<key<<endl<<endl;
    Kgen(K1,K2);

    ciphertext=make_rounds(plaintext,K1,K2,0);
    cout<<"Ciphertext:   "<<ciphertext<<endl;
    ciphertext=make_rounds(ciphertext,K1,K2,1);
    cout<<"Deciphertext: "<<ciphertext<<endl;
}

bitset<10> SimplifiedDES::P10(bitset<10> bitkey)
{
    unsigned int p[10]={3,5,2,7,4,10,1,9,8,6};
    bitset<10> pm;
    for(unsigned int i = 0; i<bitkey.size(); i++)
    {
        pm[bitkey.size()-i-1]=bitkey[bitkey.size()-(p[i])];
    }
    return pm;
}
bitset<8> SimplifiedDES::P8(bitset<10> bits)
{
    unsigned int p[8]={6, 3, 7, 4, 8, 5, 10, 9};
    bitset<8> pm;
    for(unsigned int i = 0; i<pm.size(); i++)
    {
        pm[pm.size()-i-1]=bits[bits.size()-(p[i])];
    }
    return pm;
}
bitset<10> SimplifiedDES::sshift(bitset<10> input, unsigned int rot)
{
    bitset<5> left;
    bitset<5> right;
    for(unsigned int i = 0; i<5; i++)
    {
        left[i] = input[i+5];
        right[i] = input[i];
    }
    left = left<<(rot) | left>>(left.size()-rot);
    right = right<<(rot) | right>>(right.size()-rot);
    //cout<<"Left: "<<left<<endl;
    //cout<<"Right: "<<right<<endl;
    bitset<10> foo;
    for(unsigned int i = 0; i<5; i++)
    {
        foo[i]=right[i];
        foo[i+5]=left[i];

    }
    return foo;
}

void SimplifiedDES::Kgen(bitset<8> &K1, bitset<8> &K2)
{
    bitset<10> bitkey(key);
    bitset<10> aux = P10(bitkey);
    //cout<<"P10: "<<aux<<endl;
    K1 = P8(sshift(aux,1));
    K2 = P8(sshift(aux,3));
    cout<<"K1: "<<K1<<endl;
    cout<<"K2: "<<K2<<endl<<endl;
}

bitset<8> SimplifiedDES::IP(bitset<8> bits)
{
    unsigned int p[]={2,6,3,1,4,8,5,7};
    bitset<8> pm;
    for(unsigned int i = 0; i<bits.size(); i++)
    {
        pm[bits.size()-i-1]=bits[bits.size()-(p[i])];
    }
    return pm;
}
bitset<8> SimplifiedDES::IPi(std::bitset<8> bits)
{
    unsigned int p[]={4,1,3,5,7,2,8,6};
    bitset<8> pm;
    for(unsigned int i = 0; i<bits.size(); i++)
    {
        pm[bits.size()-i-1]=bits[bits.size()-(p[i])];
    }
    return pm;
}
bitset<4> SimplifiedDES::P4(std::bitset<4> input)
{
    unsigned int p[]={2,4,3,1};
    bitset<4> pm;
    for(unsigned int i = 0; i<input.size(); i++)
    {
        pm[input.size()-i-1]=input[input.size()-(p[i])];
    }
    return pm;
}

bitset<4> SimplifiedDES::F(std::bitset<4> input, std::bitset<8> Ki)
{
    unsigned int E_P[]={4,1,2,3,2,3,4,1};
    unsigned int S0[4][4]={{1,0,3,2},
                           {3,2,1,0},
                           {0,2,1,3},
                           {3,1,3,2}};
    unsigned int S1[4][4]={{0,1,2,3},
                           {2,0,1,3},
                           {3,0,1,0},
                           {2,1,0,3}};
    bitset<8> EPbits;
    for(unsigned int i = 0; i<EPbits.size(); i++)
    {
        EPbits[EPbits.size()-i-1]=input[input.size()-(E_P[i])];
    }
    EPbits^=Ki;
    bitset<2> row1, row2, col1, col2;
    row1[1]=EPbits[7];
    row1[0]=EPbits[4];
    col1[1]=EPbits[6];
    col1[0]=EPbits[5];
    row2[1]=EPbits[3];
    row2[0]=EPbits[0];
    col2[1]=EPbits[2];
    col2[0]=EPbits[1];
    unsigned long S0row = row1.to_ulong();
    unsigned long S0col = col1.to_ulong();
    unsigned long S1row = row2.to_ulong();
    unsigned long S1col = col2.to_ulong();
    bitset<4> S0bits(S0[S0row][S0col]);
    bitset<4> S1bits(S1[S1row][S1col]);
    bitset<4> num = S0bits<<2 | S1bits;
    return P4(num);
}

bitset<8> SimplifiedDES::fk(std::bitset<8> input, std::bitset<8> Ki)
{
    bitset<4> left;
    bitset<4> right;
    for(unsigned int i = 0; i<4; i++)
    {
        left[i] = input[i+5];
        right[i] = input[i];
    }
    bitset<4> rF = F(right,Ki);
    left^=rF;
    for(unsigned int i = 0; i<4; i++)
    {
        input[i+5]=left[i];
    }
    return input;
}

bitset<8> SimplifiedDES::SW(std::bitset<8> input)
{
    return input<<(4) | input>>(4);
}

bitset<8> SimplifiedDES::make_rounds(bitset<8> input, bitset<8> K1, bitset<8> K2, int mode)
{
    bitset<8> proc;
    if(mode == 0) //encrypt
    {
        proc=fk(IP(input), K1);
        proc=SW(proc);
        proc=fk(proc,K2);
        proc=IPi(proc);
    }
    else //decript
    {
        proc=fk(IP(input), K2);
        proc=SW(proc);
        proc=fk(proc,K1);
        proc=IPi(proc);
    }
    return proc;
}
