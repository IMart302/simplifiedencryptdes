#ifndef SIMPLIFIEDDES_H
#define SIMPLIFIEDDES_H

#include <bitset>
#include <QFile>
#include <fstream>

class SimplifiedDES
{
private:
    SimplifiedDES(const SimplifiedDES& cipher);
    const SimplifiedDES& operator=(const SimplifiedDES& cipher);
    std::bitset<10> P10(std::bitset<10> bitkey);
    std::bitset<8> P8(std::bitset<10> bits);
    std::bitset<8> IP(std::bitset<8> bits);
    std::bitset<8> IPi(std::bitset<8> bits);
    std::bitset<4> P4(std::bitset<4> input);
    std::bitset<4> F(std::bitset<4> input, std::bitset<8> Ki);
    std::bitset<8> fk(std::bitset<8> input, std::bitset<8> Ki);
    std::bitset<8> SW(std::bitset<8> input);
    std::bitset<8> make_rounds(std::bitset<8> input, std::bitset<8> K1, std::bitset<8> K2, int mode);
    std::bitset<10> sshift(std::bitset<10> input,unsigned int rot);
    void Kgen(std::bitset<8> &K1, std::bitset<8> &K2);
    std::string key;
public:
    SimplifiedDES();
    explicit SimplifiedDES(std::string key);
    bool encrypt(QFile &plaintext, QFile &out);
    bool decrypt(QFile &ciphertext, QFile &out);
    bool encrypt(std::ifstream &plaintext, std::ofstream &out);
    bool decrypt(std::ifstream &ciphertext, std::ofstream &out);
    void setkey(std::string key);
    std::string getkey();
    void test();
};

#endif // SIMPLIFIEDDES_H
